var spotippos = (function() {

	/**
   * Used to get Spotippos's content
   *
   * @method fetch
   * @param url {String} url to fetch
   * @param callback {Function} Callback function to be called after succeed request
   * @example fetch('https://raw.githubusercontent.com/VivaReal/code-challenge/master/provinces.json', someFunction);
   * @private
   */

    function fetch(url, callback) {
	    var httpRequest = new XMLHttpRequest();
	    httpRequest.onreadystatechange = function() {
			// Spotippos' request should finished and status should be OK
	        if (httpRequest.readyState === 4 && httpRequest.status === 200) {
	            if(callback && typeof callback === 'function') {
					callback(JSON.parse(httpRequest.responseText));
				}
	        }
	    };
	    httpRequest.open('GET', url);
	    httpRequest.send();
	}

	/**
   * Used to get Spotippos's content
   *
   * @method createProvince
   * @param target {String} Spotippos' id
   * @param id {String} province's id
   * @param boundaries {Object} Object with all provinces boundaries
   * @example createProvince('spotippos-map', "ID", boundariesObj);
   * @private
   */

	function createProvince(target, id, boundaries) {
		var province = document.createElement('div');
		province.id = id;
		province.className = 'provinces';
		province.style.bottom = boundaries.bottomRight.y + 'px';
		province.style.left = boundaries.upperLeft.x + 'px';
		province.style.width = (boundaries.bottomRight.x - boundaries.upperLeft.x) + 'px';
		province.style.height = (boundaries.upperLeft.y - boundaries.bottomRight.y) + 'px';
		province.innerHTML = '<span class="name">' + id + '<span/>';
		document.getElementById(target).appendChild(province);
	}

	/**
	* Used to load Spotippos
	*
	* @method init
	* @param target {String} province's container id
	* @example init('spotippos-map');
	* @public
	*/

	function init(target) {
		fetch('https://raw.githubusercontent.com/VivaReal/code-challenge/master/provinces.json', function(data) {
			if(data) {
				for (var key in data) {
					createProvince(target, key, data[key].boundaries);
				}
			}
		});
	}

	return {
		init: init
	}
})();

spotippos.init('spotippos-map');
